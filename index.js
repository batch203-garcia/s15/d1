console.log("Hello, World!");
console.log("Hello, B203!");

//COMMENTS

//This is a single line comment - ctr + / 

/* Multi Line
Comment */

//Syntax and Statements
/*  
    1. Statements are instructions for a computer to perform
    2. Sets of rules that decribes how statements are constructed.
*/

//Variable
/* 
    -used to contain data
    -sysntax in declaring a variables
    -let/const variableName;
*/

let myVariable = "Hello!";
let MYvariable = "Hi!";
console.log(myVariable);

let productName = '"desktop computer"';

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

productName = "laptop";
console.log(productName);

let friend = "Kate";
friend = "Jane";

console.log(friend);

/* interest = 4.489 */ //err const var

//re-assigning and initializing variable

let supplier;

supplier = "John Smith Trading"; //firt time value = init
console.log(supplier);

supplier = "Zuitt Store"; //2nd time or more value = re-assigning
console.log(supplier);

const pi = 3.1416;
/* pi = 3.1416; */ // err: const value should be init during declarration
console.log(pi);

/* a = 5;
console.log(a);
var a; */

//Multiple variable declarations

let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productName);

//reserved keyword
/* const let = "hello";
console.log(let); */

// [SECTION] Data Types

/* 
1. All values enclosed to a double quote or single quote.
*/
let country = 'Philippines';
let province = "Metro Manila"

//concatenation

let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// \n break line - escape character
let mailAddress = "Metro Manila\nPhilippines"
console.log(mailAddress);

let message = 'John\'s employees went home early.'
console.log(message);

//whole numbers
let headcount = 26;
console.log(headcount);

//Decimal numbers/fraction
let grade = 98.7;
console.log(grade);

//Exponential Notation

let planetDistance = 2e10;
console.log(planetDistance);

//combine num and str
console.log("John's grade last quarter is " + grade);

//boolean - true or false
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried " + isMarried);
console.log("isGoodConduct " + isGoodConduct);

//Arrays - store multiple values with same data type
//Array Syntax - let/const arrayName = [elementA, ElemetB, elemetC ....];

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//different data types
let details = ["John", "Smith", 32, true]; //can be, but confusing
console.log(details);

//Objects
//Syntax - let/const objectName = { propertyA: value, propertyB: value }

let person = {
    fullNmae: "Juan Dela Cruz",
    age: 35,
    contact: ["+63917 123 4567", "8123 4567"],
    address: {
        houseNumber: "345",
        city: "Manila"
    }
}
console.log(person);
console.log(person.address.houseNumber + " " + person.address.city);

//Abstract Object

let myGrades = {
    firstGrading: 98.7,
    secondGrading: 92.1,
    thirdGrading: 90.2,
    fourthGrqading: 94.6
}

console.log(myGrades);
console.log(myGrades.secondGrading);

//check/determine data type of a variable value
console.log(typeof myGrades);
console.log(typeof grades);

//Constant Objects and Arrays
const anime = ["one piece", "one punch man", "attack on titan"];
anime[0] = "kimetsu no yaiba";

console.log(anime);

//Null - used intentionally = no value